# APSO<br><br>Alternative python script organizer for OpenOffice and LibreOffice

Based on an [original script from Hanya](https://forum.openoffice.org/en/forum/viewtopic.php?p=242221#p242221), APSO is an extension that will install a macro organizer dedicated to python scripts.

<h3 style="color:#ff5555;">! Warning !</h3>
Release 1.3.0 introduces a new configuration design. User upgrading from versions 1.2.x or lower will have to re-enter his preferences in Apso options dialog.

### Prerequisite
On some systems (like Ubuntu), the python script provider for LibreOffice is only available as a separate package (see [here](https://wiki.documentfoundation.org/Macros/Python_Guide/Introduction#Installation)).

### Features
- localisation (by now EN, DE, FR, IT, HU thanks to Zizi64, NL thanks to Dick Groskamp, ES thanks to Francisco Gracia and JA thanks to nogajun)
- embedding in and extracting scripts from documents
- binding the extension with one's favorite code editor (thanks to [another script from Hanya](http://hermione.s41.xrea.com/pukiwiki/index.php?OOobbs2%2F194&word=editorkicker), see Options below)
- quick edition of embedded scripts (but only in the context of the APSO extension, be careful here)
- automatic update once installed
- context menu for quick access to node actions (from 1.3.0)

### How to use it
Download and install the latest release like any other extension: double-click the oxt file or import it from the extension manager (menu *Tools -> Extension Manager...*), then restart the program.

The extension adds a new element *Organize python scripts* under menu *Tools/Macros* with default shortcut *Alt+Shift+F11*:

![apso_menu](img/apso_menu.png)

This new item or shortcut opens a dialog box that shows all python scripts and provides various actions:

![apso_dialog](img/apso_dialog.png)

The **Execute**  button launches the selected macro.
Actions available under **Menu** button change according the selection:
- create module or library if selection is a container (*My Macros*, *Office Macros* or *document*)
- Edit, rename, delete a module or library
- Embed module from application (*My Macros*, *Office Macros*) into current document
- Substitute an embedded module with an external file
- Export an embedded module

### Helpers
APSO comes with a small library `apso_utils` that contains some helpers for debugging purpose during macro writing.
These helpers are four functions named *msgbox*, *xray*, *mri* and *console* and can be imported by a classic import statement like 
```python
from apso_utils import msgbox
```

function | description
:---: | ---
msgbox | Like the oobasic build-in function *msgbox*, but simplified as only intended for quick debugging.<br>**Signature**: `msgbox(message, title='Message', boxtype='message', buttons=1, win=None)`
xray | Launches the introspection tool [Xray](http://www.openoffice.org/fr/Documentation/Basic/) by Bernard Marcelly (must be installed first).<br>**Signature**: `xray(obj)`
mri | Launches the introspection tool [MRI](https://extensions.openoffice.org/en/project/MRI) by Hanya (must be installed first).<br>**Signature**: `mri(target)`
console | Emulates, as close as possible, the python interactive shell. What one can do with such a tool:<br />- interact directly with a document and test "live" the methods of the uno api (maybe also useful for other languages, such basic);<br />- quickly verify python expressions in the context of the embedded interpreter;<br />- display the output of a script running in parallel or started from the shell (all modules visible from the office program can be imported);<br />- debug a script using pdb's runcall function (will not work with scripts embedded in documents);<br />- get quick info on any function using the builtin help() command;<br />- explore living uno objects with introspection tools such as MRI or Xray (if installed);<br />- ....<br>**Signature**: `console(**kwargs)`, where optional *kwargs* are intended to overwrite default gui values (BACKGROUND, FOREGROUND...) or to pass caller's locales and/or globals to the console context.


**_Example_**
```python
from apso_utils import console, msgbox, mri, xray
def testhelpers():
    ## simple console:
    # console()

    ## black&white console:
    # console(BACKGROUND=0x0, FOREGROUND=0xFFFFFF)

    ## pass parent window to get non-modal top console:
    # doc = XSCRIPTCONTEXT.getDocument()
    # console(parent=doc.CurrentController.ComponentWindow)

    ## load currents variables into console namespace:
    loc = locals()
    loc.update(globals())
    console(loc=loc)

    ## introspection tools and message box:
    doc = XSCRIPTCONTEXT.getDocument()
    # xray(doc)
    msgbox(doc.Title)
    mri(doc)
```

<img src="img/apso_console.jpg" width="400">

**_Tips_**

- The *console* macro is accessible without opening the APSO dialog.

- If someone wants to import other helpers out of APSO dialog, he has to instanciate APSO service:
```python
def entryfunc(event=None):
    ctx = XSCRIPTCONTEXT.getComponentContext()
    ctx.ServiceManager.createInstance("apso.python.script.organizer.impl")
    # now we can import apso_utils library
    from apso_utils import mri
    mri(ctx)
```

### Options
The extension option page is available under *Tools -> Extension Manager -> APSO -> Options*.

By default, APSO uses the system defined editor. To edit scripts with a specific editor, provide the executable full path in the "Editor" field.

To allow the opening at a given line and column offset when relevant, enter in the "Options" field the command line syntax corresponding to the choosen editor, using the placeholders {FILENAME}, {ROW} and {COL} (they will be replaced in due time).
Example for Emacs: `+{ROW}:{COL} {FILENAME}`.
Example for Sublime Text: `{FILENAME}:{ROW}:{COL}.`

A third option allows specifying the default content on any new module.

Three advanced options are available in the *Expert configuration* section of LibreOffice (menu *Tools -> Options -> LibreOffice -> Advanced*):
- `SaveLastScript`, if true, keeps history of the last run script (added in 1.3.0);
- `SaveTreeState`, if true, keeps history of all expanded nodes on last run script (added in 1.3.0);
- `UnloadChildOnCollapse`, if true, collapses the requested node and all its children; if false, collapses only the requested node (added in 1.3.0); 

### Debugger
From version 1.1.5, Apso comes with an integreted basic debugger:

![apso_menu](img/apso_debug.png)

The debugger can be launched from the Apso **Menu** button when a function is selected in the main window.

Once the debugger opened, one can execute the code step by step, enter any called function, return to the caller, etc.

Break points are not supported, but you can jump ahead by double clicking any line below the current one within the same function.

You can check any caller state and scope by double clicking it in the "Stack" window (bottom left). If you have MRI or Xray installed, you can inspect any UNO variables by double clicking it in the "Scope" window (bottom right).

When you open a console window from the tool bar, the global and local scope will be automatically loaded.

If you edit the script while the debugger is running, changes will be taken in account after clicking "Restart".

The debugger uses the font name and height defined for the Basic IDE.
